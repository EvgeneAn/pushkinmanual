// получить все ссылки на якоря
const anchors = document.querySelectorAll('a[href^="#"]')

for (let anchor of anchors) {
    // определить новый обработчик события click
    anchor.addEventListener('click', function(e) {
        // установить действие по умолчанию (отменит переход к якорю)
        e.preventDefault()

        // найти блок, к которому будет выполняться прокручивание
        const blockID = anchor.getAttribute('href').substr(1)

        // прокрутить текущий контейнер к блоку
        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}

// прокрутка наверх
arrowTop.onclick = function() {
    document.querySelector('body').scrollIntoView({
        behavior: 'smooth'
    })
}

// скрытие/появление кнопки для прокрутки наверх
window.addEventListener('scroll', function() {
    arrowTop.hidden = (pageYOffset < document.
        documentElement.clientHeight);
})