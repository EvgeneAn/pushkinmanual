let validName = false;
let validEmail = false;
let validPassword = false;
let validPassword2 = false;

function nameValid(event) {
    let value = event.target.value;
    let reg = /^[a-zа-я0-9_ ]+$/i;
    validName = reg.test(value);
    showError(event.target, validName);
}

function emailValid(event) {
    let value = event.target.value;
    let reg = /^[a-z0-9][a-z0-9\._+%-]*[a-z0-9]@[a-z]+\.[a-z]{2,}$/i;
    validEmail = reg.test(value);
    showError(event.target, validEmail);
}

function passwordValid(event) {
    let value = event.target.value;
    let reg = /^[a-z0-9=+._?#%@!*-]{8,}$/i;
    validPassword = reg.test(value);
    showError(event.target, validPassword);
}

function password2Valid(event) {
    let password2 = event.target.value;
    let password = document.querySelector('input[name="password"]').value;

    let errorMessage = document.querySelector('input[name="password2"]+.form-error');
    validPassword2 = password2 === password
    errorMessage.hidden = validPassword2;
}

// Отображает/скрывает текст ошибки
function showError(target, flag) {
    let errorMessage = document.querySelector('input[name="'
        + target.name + '"]+.form-error');

    errorMessage.hidden = flag;
}


// Привязка обработчиков событий
document.querySelector('.form-control input[name="username"]')
    .addEventListener('input', (event) => nameValidator = nameValid(event));

document.querySelector('.form-control input[name="email"]')
    .addEventListener('input', (event) => emailValidator = emailValid(event));

document.querySelector('.form-control input[name="password"')
    .addEventListener('input', (event) => passwordValidator = passwordValid(event));

document.querySelector('.form-control input[name="password2"]')
    .addEventListener('input', (event) => password2Validator = password2Valid(event));

document.querySelector('.form-register input[type="submit"]')
    .addEventListener('click', function(event) {
        event.preventDefault();

        console.log(validName, validEmail, validPassword, validPassword2)

        if (validName && validEmail && validPassword && validPassword2)
            alert('Все поля валидны');
        else
            alert('Не все поля валидны');
    });