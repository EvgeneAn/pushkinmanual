let sub_menus = document.getElementsByClassName('parent-menu');

for (let sub_menu of sub_menus) {
    sub_menu.addEventListener("mouseenter", showSub);
    sub_menu.addEventListener("mouseleave", hideSub);
}

function showSub(event) {
    let sub_menu = event.target.querySelector('ul');
    sub_menu.classList.replace("sub-menu-hidden", "sub-menu-visible");
}

function hideSub(event) {
    let sub_menu = event.target.querySelector('ul');
    sub_menu.classList.replace("sub-menu-visible", "sub-menu-hidden");
}